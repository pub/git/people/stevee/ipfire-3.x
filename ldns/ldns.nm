###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = ldns
version    = 1.8.3
release    = 1

groups     = Networking/DNS
url        = http://www.nlnetlabs.nl/projects/ldns/
license    = BSD
summary    = Low-level DNS(SEC) library with API

description
	The goal of ldns is to simplify DNS programming in C. ldns supports
	all low-level DNS and DNSSEC operations. It also defines a higher
	level API which allows a programmer to for instance create or sign
	packets.
end

source_dl  = https://www.nlnetlabs.nl/downloads/%{name}/

build
	requires
		ca-certificates
		doxygen
		gcc-c++
		libpcap-devel
		openssl-devel
	end

	prepare_cmds
		sed -i "s/@includedir@/@includedir@\/ldns/" \
			packaging/libldns.pc.in
	end

	configure_options += \
		--disable-gost \
		--enable-ecdsa \
		--with-ca-file=/etc/pki/tls/certs/ca-bundle.trust.crt \
		--with-ca-path=/etc/pki/tls/certs/ \
		--with-trust-anchor=%{sharedstatedir}/unbound/root.key
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL

		files += %{bindir}/ldns-config
		files += %{mandir}/man1/ldns-config.1*
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
