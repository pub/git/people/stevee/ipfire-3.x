###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = multipath-tools
version    = 0.9.4
release    = 1

groups     = System/Base
url        = http://christophe.varoqui.free.fr/
license    = GPL+
summary    = Tools to manage multipath devices using device-mapper.

description
	This package provides tools to manage multipath devices by
	instructing the device-mapper multipath kernel module what to do.
end

source_dl  = https://github.com/opensvc/%{name}/archive/refs/tags/%{version}.tar.gz#/
sources    = %{thisapp}.tar.gz

build
	requires
		gzip
		json-c-devel
		libaio-devel
		libdevmapper-devel
		libudev-devel
		liburcu-devel
		ncurses-devel
		readline-devel
	end

	make_build_targets += kpartx LIB=%{lib} LDFLAGS="%{LDFLAGS} -ldevmapper"

	# Install everything to the correct locations.
	make_install_targets += \
		-C kpartx \
		bindir=%{sbindir} \
		syslibdir=%{libdir} \
		mandir=%{mandir} \
		libdir=%{libdir}/multipath \
		unitdir=%{unitdir}
end

packages
	package kpartx
		summary = Partition manager for device-mapper devices.
		description
			kpartx manages partition creation and removal for device-mapper devices.
		end

		files
			/usr/lib/udev
			%{sbindir}/kpartx
			%{mandir}/man8/kpartx.8*
		end
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
