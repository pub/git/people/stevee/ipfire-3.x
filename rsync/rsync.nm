###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = rsync
version    = 3.2.7
release    = 1

groups     = Applications/Internet
url        = https://rsync.samba.org/
license    = GPLv3+
summary    = A program for synchronizing files over a network.

description
	Rsync uses a reliable algorithm to bring remote and host files into
	sync very quickly. Rsync is fast because it just sends the differences
	in the files over the network instead of sending the complete
	files. Rsync is often used as a very powerful mirroring process or
	just as a more capable replacement for the rcp command. A technical
	report which describes the rsync algorithm is included in this
	package.
end

source_dl  = https://rsync.samba.org/ftp/rsync/

build
	requires
		libacl-devel
		libattr-devel
		lz4-devel
		openssl-devel
		popt-devel
		zlib-devel
		zstd-devel

		# Testsuite
		%{bindir}/setfacl
	end

	configure_options += \
		--disable-debug \
		--without-included-popt \
		--without-included-zlib \
		--enable-openssl \
		--enable-zstd \
		--disable-xxhash

	test
		make check || :
	end

	install_cmds
		# Remove rsync-ssl which we don't want
		rm -vf %{BUILDROOT}{%{bindir}/rsync-ssl,%{mandir}/man1/rsync-ssl.*}
	end
end

packages
	package %{name}

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
