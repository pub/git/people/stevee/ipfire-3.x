###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = shadow-utils
version    = 4.13
release    = 2
thisapp    = shadow-%{version}

groups     = System/Base
url        = https://github.com/shadow-maint/shadow/
license    = GPLv2+
summary    = Utilities to deal with user accounts.

description
	The shadow-utils package includes the necessary programs
	for converting UNIX password files to the shadow password
	format, plus programs for managing user and group accounts.
end

source_dl  = https://github.com/shadow-maint/shadow/archive/%{version}.tar.gz#/

build
	requires
		autoconf
		automake
		bison
		docbook-xsl
		flex
		gettext-devel
		gnome-doc-utils
		libacl-devel
		libattr-devel
		libcap-devel
		libtool
		libxcrypt-devel
		pam-devel
	end

	prepare_cmds
		autoreconf -vfi
	end

	configure_options += \
		--disable-account-tools-setuid \
		--enable-man \
		--with-libpam \
		--with-group-name-max-length=32 \
		--with-bcrypt \
		--with-yescrypt \
		--without-audit \
		--without-selinux \
		--without-su \
		--with-fcaps

	install_cmds
		rm -vf \
			%{BUILDROOT}/{%{bindir},%{sbindir},%{mandir}/*}/{chage,chfn,chsh,expiry,login,logoutd,newgrp,nologin,sg,vigr,vipw}*
	end
end

packages
	package %{name}
		conflicts
			pam < 1.1.0-4
		end

		configfiles
			/etc/default/useradd
		end
	end

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
