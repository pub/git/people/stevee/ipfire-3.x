###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = ccache
version    = 4.7.4
release    = 1

groups     = Development/Compilers
url        = https://ccache.dev
license    = GPLv2+
summary    = C/C++ compiler cache.

description
	ccache is a compiler cache.  It acts as a caching pre-processor to
	C/C++ compilers, using the -E compiler switch and a hash to detect
	when a compilation can be satisfied from cache.  This often results in
	a 5 to 10 times speedup in common compilations.
end

source_dl  = https://github.com/ccache/ccache/releases/download/v%{version}/

build
	requires
		cmake
		perl
		zlib-devel
		zstd-devel
	end

	CCACHE_CACHE = %{localstatedir}/cache/ccache

	build
		%{cmake} . \
			-DREDIS_STORAGE_BACKEND=OFF

		make %{PARALLELISMFLAGS}
	end

	#test
	#	# XXX Disabled because the testsuite removes libzstd.so.1
	#	make check
	#end

	install_cmds
		CCACHE_PLATFORM_TARGET=$(echo %{DISTRO_MACHINE} | sed -e "s/-gnu//g")

		mkdir -pv %{BUILDROOT}%{libdir}/ccache
		for i in gcc g++ cc c++; do \
			ln -svf ../../bin/ccache %{BUILDROOT}%{libdir}/ccache/${i}; \
			ln -svf ../../bin/ccache %{BUILDROOT}%{libdir}/ccache/${CCACHE_PLATFORM_TARGET}-${i}; \
		done

		mkdir -pv %{BUILDROOT}/etc/profile.d
		sed -e "s|@LIBDIR@|%{libdir}|g" -e "s|@CACHEDIR@|%{CCACHE_CACHE}|g" \
			< %{DIR_SOURCE}/ccache.sh > %{BUILDROOT}/etc/profile.d/ccache.sh

		mkdir -pv -m770 %{BUILDROOT}/%{CCACHE_CACHE}
	end
end

packages
	package %{name}

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
