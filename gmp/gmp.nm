###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = gmp
version    = 6.2.1
release    = 1.1

groups     = System/Libraries
url        = https://gmplib.org/
license    = LGPLv3+
summary    = A GNU arbitrary precision library.

description
	The gmp package contains GNU MP, a library for arbitrary precision
	arithmetic, signed integers operations, rational numbers and floating
	point numbers. GNU MP is designed for speed, for both small and very
	large operands. GNU MP is fast because it uses fullwords as the basic
	arithmetic type, it uses fast algorithms, it carefully optimizes
	assembly code for many CPUs' most common inner loops, and it generally
	emphasizes speed over simplicity/elegance in its operations.
end

source_dl += https://gmplib.org/download/gmp/ ftp://ftp.gnu.org/gnu/gmp/
sources    = %{thisapp}.tar.xz

build
	requires
		gcc-c++
		m4
	end

	# Set ABI to "64" because we currently only
	# build for architectures which supports 64 Bits.
	export ABI = 64

	configure_options += \
		--enable-cxx \
		--enable-mpbsd \
		--disable-static

	test
		LD_LIBRARY_PATH=%{DIR_APP}/.libs make check
	end
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
