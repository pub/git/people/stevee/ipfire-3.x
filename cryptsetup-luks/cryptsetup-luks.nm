###############################################################################
# IPFire.org    - An Open Source Firewall Solution                            #
# Copyright (C) - IPFire Development Team <info@ipfire.org>                   #
###############################################################################

name       = cryptsetup-luks
version    = %{ver_maj}.1
ver_maj    = 2.6
release    = 1

groups     = System/Filesystems
url        = https://gitlab.com/cryptsetup/cryptsetup
license    = GPLv2+
summary    = A utility for setting up encrypted filesystems.

description
	This package contains cryptsetup, a utility for setting up
	encrypted filesystems using Device Mapper and the dm-crypt target.
end

source_dl  = https://www.kernel.org/pub/linux/utils/cryptsetup/v%{ver_maj}/
sources    = cryptsetup-%{version}.tar.xz

build
	requires
		json-c-devel
		libdevmapper-devel
		libgcrypt-devel
		libgpg-error-devel
		libuuid-devel
		openssl-devel
		popt-devel
	end

	DIR_APP = %{DIR_SRC}/cryptsetup-%{version}

	configure_options += \
		--disable-asciidoc \
		--disable-ssh-token \
		--disable-static
end

packages
	package %{name}

	package %{name}-devel
		template DEVEL
	end

	package %{name}-debuginfo
		template DEBUGINFO
	end
end
